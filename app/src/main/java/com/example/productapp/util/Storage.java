package com.example.productapp.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.productapp.model.Product;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Storage {
    private SharedPreferences sharedPreferences;
    private List<Product> products = new ArrayList<>();

    public Storage(Context context) {
        sharedPreferences = context.getSharedPreferences("cart", Context.MODE_PRIVATE);
    }

    public void writeValues(int idProduct, int quantity){
        //Ajoute et commit
        if(sharedPreferences.contains(String.valueOf(idProduct))){
            int qty = (int) sharedPreferences.getAll().get(String.valueOf(idProduct));
            quantity += qty;

        }
        sharedPreferences.edit().putInt(String.valueOf(idProduct), quantity).commit();
    }

    public Map getAll(){
        return sharedPreferences.getAll();
    }

    public void createProductsOrdered(Product product){
        products.add(product);
    }

    public List<Product> getProducts() {
        return products;
    }

    public void removeAll(){
        sharedPreferences.edit().clear().commit();
        products = new ArrayList<>();
    }

}
