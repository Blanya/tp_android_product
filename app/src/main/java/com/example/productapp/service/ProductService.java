package com.example.productapp.service;

import com.example.productapp.model.Product;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface ProductService {
    @GET("products/")
    public Call<List<Product>> getProducts();

    @GET("products/{id}")
    public Call<Product> getProductById(@Path("id") int id);

}
