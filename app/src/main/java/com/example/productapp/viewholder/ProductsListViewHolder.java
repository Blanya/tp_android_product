package com.example.productapp.viewholder;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavDirections;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.RecyclerView;

import com.example.productapp.ProductsFragmentDirections;
import com.example.productapp.R;
import com.example.productapp.model.Product;
import com.example.productapp.service.ProductService;
import com.example.productapp.util.RetrofitClient;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class ProductsListViewHolder extends RecyclerView.ViewHolder {

    private Fragment _fragment;
    private ProductService productService;
    private RetrofitClient retrofitClient;
    private ImageView imageView;
    private TextView titleTextView;
    private TextView priceTextView;
    private TextView qtyTextView;

    public ProductsListViewHolder(@NonNull View itemView, Fragment fragment) {
        this(itemView);
        _fragment = fragment;
//        productService = retrofitClient.getInstance().getProductService();
    }

    public ProductsListViewHolder(@NonNull View itemView) {
        super(itemView);
        imageView = itemView.findViewById(R.id.img_item);
        titleTextView = itemView.findViewById(R.id.title_item);
        priceTextView = itemView.findViewById(R.id.price_item);
        qtyTextView = itemView.findViewById(R.id.qty_item);
    }

    public static ProductsListViewHolder create(ViewGroup parent, Fragment fragment) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_item, parent, false);
        return new ProductsListViewHolder(view, fragment);
    }

    public void display(Product product){

        if(product.getQty() != 0){
            qtyTextView.setText("Qty : " + String.valueOf(product.getQty()));
        }

        Picasso.get().load(product.getImg()).into(imageView);
        titleTextView.setText(product.getTitle());
        priceTextView.setText(String.valueOf(product.getPrice()));

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavDirections navDirections = com.example.productapp.ProductsFragmentDirections.actionProductsToDetails(getAdapterPosition() + 1);
                NavHostFragment.findNavController(_fragment).navigate(navDirections);
            }
        });

    }

/*    private void downloadImage(Product product) {
        Bitmap bitmap = null;
        try {
            URL url = new URL(product.getImg());
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            InputStream inputStream = connection.getInputStream();
            bitmap = BitmapFactory.decodeStream(inputStream);
            imageView.setImageBitmap(bitmap);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }*/

}
