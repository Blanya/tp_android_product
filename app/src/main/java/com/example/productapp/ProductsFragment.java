package com.example.productapp;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.productapp.adapter.ProductsListAdapter;
import com.example.productapp.databinding.FragmentProductsBinding;
import com.example.productapp.model.Product;
import com.example.productapp.service.ProductService;
import com.example.productapp.util.RetrofitClient;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductsFragment extends Fragment {

    private FragmentProductsBinding binding;
    private RetrofitClient retrofitClient;
    private ProductService productService;
    private ProductsListAdapter adapter;
    public ProductsFragment() {
        // Required empty public constructor
        productService = retrofitClient.getInstance().getProductService();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentProductsBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        adapter = new ProductsListAdapter(new ProductsListAdapter.ProductDiff(), ProductsFragment.this);
        binding.recyclerProductsList.setAdapter(adapter);
        binding.recyclerProductsList.setLayoutManager(new LinearLayoutManager(getContext()));

        productService.getProducts().enqueue(new Callback<List<Product>>() {
            @Override
            public void onResponse(Call<List<Product>> call, Response<List<Product>> response) {
                List<Product> products = response.body();
                adapter.submitList(products);
            }

            @Override
            public void onFailure(Call<List<Product>> call, Throwable t) {
                Toast.makeText(getContext(),t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }
}