package com.example.productapp.adapter;

import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;

import com.example.productapp.model.Product;
import com.example.productapp.viewholder.ProductsListViewHolder;

public class ProductsListAdapter extends ListAdapter<Product, ProductsListViewHolder> {
    private Fragment _fragment;

    public ProductsListAdapter(@NonNull DiffUtil.ItemCallback<Product> diffCallback, Fragment fragment) {
        this(diffCallback);
        _fragment = fragment;
    }

    protected ProductsListAdapter(@NonNull DiffUtil.ItemCallback<Product> diffCallback) {
        super(diffCallback);
    }

    @NonNull
    @Override
    public ProductsListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return ProductsListViewHolder.create(parent, _fragment);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductsListViewHolder holder, int position) {
        Product product = getItem(position);
        holder.display(product);
    }

    public static class ProductDiff extends DiffUtil.ItemCallback<Product> {

        @Override
        public boolean areItemsTheSame(@NonNull Product oldItem, @NonNull Product newItem) {
            return oldItem == newItem;
        }

        @Override
        public boolean areContentsTheSame(@NonNull Product oldItem, @NonNull Product newItem) {
            return oldItem.getTitle().equals(newItem.getTitle()) &&
                    oldItem.getImg().equals(newItem.getImg()) &&
                    oldItem.getPrice() == newItem.getPrice() &&
                    oldItem.getCategory().equals(newItem.getCategory());
        }
    }

}
