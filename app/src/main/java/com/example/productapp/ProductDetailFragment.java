package com.example.productapp;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.example.productapp.databinding.FragmentProductDetailBinding;
import com.example.productapp.model.Product;
import com.example.productapp.service.ProductService;
import com.example.productapp.util.RetrofitClient;
import com.example.productapp.util.Storage;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductDetailFragment extends Fragment {

    private FragmentProductDetailBinding binding;
    private ProductService productService;
    private Storage storage;
    private int quantity = 0;
    private int id;
    public ProductDetailFragment() {
        // Required empty public constructor
        productService = RetrofitClient.getInstance().getProductService();
//        storage = new Storage(getContext());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            ProductDetailFragmentArgs args = ProductDetailFragmentArgs.fromBundle(getArguments());
            id = args.getIdProduct();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentProductDetailBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        productService.getProductById(id).enqueue(new Callback<Product>() {
            @Override
            public void onResponse(Call<Product> call, Response<Product> response) {
                Product product = response.body();
                binding.productCardTitle.setText(product.getTitle());
                binding.productCardCategory.setText(product.getCategory());
                binding.productCardDescription.setText(product.getDescription());
                binding.productCardPrice.setText(String.valueOf(product.getPrice()));
                binding.productCardRating.setRating((float) product.getRating().getRate());
                Picasso.get().load(product.getImg()).into(binding.productCardImg);

                List<Integer> quantities = new ArrayList<>();
                //Quantity
                for (int i = 1; i <= 20; i++) {
                    quantities.add(i);
                }

                ArrayAdapter adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, quantities);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                binding.productCardSpinner.setAdapter(adapter);

                binding.productCardSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        quantity = (int) parent.getItemAtPosition(position);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

                binding.prouctCardAdd.setOnClickListener(event-> {
                    if(quantity != 0){
                        storage = new Storage(getContext());
                        storage.writeValues(product.getId(), quantity);
                        Toast.makeText(getContext(),"Article ajouté !", Toast.LENGTH_LONG).show();

                        NavHostFragment.findNavController(ProductDetailFragment.this).popBackStack();
                    }
                    else
                        Toast.makeText(getContext(),"Veuillez sélectionner une quantité", Toast.LENGTH_LONG).show();
                });
            }

            @Override
            public void onFailure(Call<Product> call, Throwable t) {
                Toast.makeText(getContext(),t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }
}