package com.example.productapp;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.productapp.adapter.ProductsListAdapter;
import com.example.productapp.databinding.FragmentCartBinding;
import com.example.productapp.model.Product;
import com.example.productapp.service.ProductService;
import com.example.productapp.util.RetrofitClient;
import com.example.productapp.util.Storage;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CartFragment extends Fragment {

    private FragmentCartBinding binding;
    private ProductService productService;
    private ProductsListAdapter adapter;
    private Storage storage;

    public CartFragment() {
        // Required empty public constructor
        productService = RetrofitClient.getInstance().getProductService();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentCartBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        adapter = new ProductsListAdapter(new ProductsListAdapter.ProductDiff(), CartFragment.this);
        binding.recyclerProductsCart.setAdapter(adapter);
        binding.recyclerProductsCart.setLayoutManager(new LinearLayoutManager(getContext()));

        storage = new Storage(getContext());

        Map storageValues = storage.getAll();

        if(storageValues.size()<1){
            binding.cartTextView.setText("Aucun article dans le panier");
        }else {
            List<String> productsId = new ArrayList<>();
            List<Integer> quantities = new ArrayList<>();

            for (Object id: storageValues.keySet()) {
                productsId.add((String) id);
            }

            for (Object qty: storageValues.values()) {
                quantities.add((Integer) qty);
            }

            List<Product> p = new ArrayList<>();
            adapter.submitList(p);


            for (int i = 0; i < productsId.size(); i++) {
                int j = i;
                productService.getProductById(Integer.parseInt(productsId.get(i))).enqueue(new Callback<Product>() {
                    @Override
                    public void onResponse(Call<Product> call, Response<Product> response) {
                        Product product = response.body();
                        product.setQty(quantities.get(j));
                        storage.createProductsOrdered(product);
                        if(j == (productsId.size()-1)){
                            adapter.submitList(storage.getProducts());
                            double price = 0;
                            for (Product p :storage.getProducts()){
                                price += (p.getPrice() * p.getQty());
                            }
                            binding.cartPrice.setText(String.valueOf(price) + " euros");
                        }
                    }

                    @Override
                    public void onFailure(Call<Product> call, Throwable t) {

                    }
                });

            }


            binding.cartDelete.setOnClickListener(e ->{
                storage.removeAll();
                System.out.println(storage.getAll());
                Toast.makeText(getContext(),"Panier vidé !", Toast.LENGTH_LONG).show();

                NavHostFragment.findNavController(CartFragment.this).popBackStack();

            });
        }
    }

}
